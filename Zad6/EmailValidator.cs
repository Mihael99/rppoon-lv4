﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad6
{
    class EmailValidator : IEmailValidatorService
    {
        private bool ContainsAtAndDomain(string email)
        {
            return email.Contains("@") && (email.EndsWith(".com") || email.EndsWith(".hr"));
        } 
        public bool IsValidAddress(string email)
        {
            return ContainsAtAndDomain(email);
        }
        
    }
}
