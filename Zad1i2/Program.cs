﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1i2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("E:\\file.csv");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            double[] rowAverages = adapter.CalculateAveragePerRow(data);
            double[] columnAverages = adapter.CalculateAveragePerColumn(data);
            for (int i = 0; i < rowAverages.Length; i++)
            {
                Console.WriteLine((i + 1) + ".Row:");
                Console.WriteLine(rowAverages[i]);       
            }
            for (int i = 0; i < columnAverages.Length; i++)
            {
                Console.WriteLine((i + 1) + ".Column:");
                Console.WriteLine(columnAverages[i]);
            }
        }
    }
}
