﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5
{
    class DiscountedItem: RentableDecorator
    {
        private double Discount;
        public DiscountedItem(IRentable rentable, double discount): base(rentable)
        {
            this.Discount = discount;
    }
        public override double CalculatePrice() {
            return base.CalculatePrice() - ((this.Discount/100)*base.CalculatePrice()); 
        }
        public override String Description{
            get
            {
                return base.Description+" now at " + Discount + "% off!";
            }
}
    }
}
