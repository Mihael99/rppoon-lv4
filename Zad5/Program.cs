﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> ToRent = new List<IRentable>();
            ToRent.Add(new Book("Game Of Thrones"));
            ToRent.Add(new Video("The Matrix"));
            RentingConsolePrinter PrinterOne = new RentingConsolePrinter();
            PrinterOne.DisplayItems(ToRent);
            PrinterOne.PrintTotalPrice(ToRent);
            ToRent.Add(new HotItem(new Book("Lord Of The Rings")));
            ToRent.Add(new HotItem(new Video("Harry Potter")));
            PrinterOne.DisplayItems(ToRent);
            PrinterOne.PrintTotalPrice(ToRent);
            List<IRentable> flashSale = new List<IRentable>();
            foreach(IRentable rentable in ToRent)
            {
                flashSale.Add(new DiscountedItem(rentable, 10));
            }
            PrinterOne.DisplayItems(flashSale);
            PrinterOne.PrintTotalPrice(flashSale);
        }
    }
}
