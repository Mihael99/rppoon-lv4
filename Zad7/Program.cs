﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad7
{
    class Program
    {
        static void Main(string[] args)
        {
            UserEntry entry;
            RegistrationValidator registrationValidator = new RegistrationValidator(8);
            do
            {
                entry = UserEntry.ReadUserFromConsole();
            } while (registrationValidator.IsUserEntryValid(entry) != true);
        }
    }
}
