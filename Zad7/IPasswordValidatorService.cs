﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad7
{
    interface IPasswordValidatorService
    {
        bool IsValidPassword(String candidate);
    }
}
