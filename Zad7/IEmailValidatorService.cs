﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad7
{
    interface IEmailValidatorService
    {
        bool IsValidAddress(String candidate);
    }
}
