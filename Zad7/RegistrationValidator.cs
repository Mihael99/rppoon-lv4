﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad7
{
    class RegistrationValidator: IRegistrationValidator
    {
        private PasswordValidator passwordValidator;
        private EmailValidator emailValidator;
        public RegistrationValidator(int minLength)
        {
            this.passwordValidator = new PasswordValidator(minLength);
            this.emailValidator = new EmailValidator();
        }
        public bool IsUserEntryValid(UserEntry entry)
        {
            if(passwordValidator.IsValidPassword(entry.Password) && emailValidator.IsValidAddress(entry.Email))
            {
                return true;
            }
            return false;
        }
    }
}
