﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> ToRent = new List<IRentable>();
            ToRent.Add(new Book("Game Of Thrones"));
            ToRent.Add(new Video("The Matrix"));
            RentingConsolePrinter PrinterOne = new RentingConsolePrinter();
            PrinterOne.DisplayItems(ToRent);
        }
    }
}
